# Compression de fichier

## Modalités 

Le TP est à rendre au plus tard le 13 décembre 2020 à 23h59 via la plateforme Moodle. Il est à réaliser en binôme exclusivement, durant le temps libre. 

### Travail à rendre

Il vous est demandé un document synthétique tenant sur au maximum trois feuilles recto verso, soit six pages, où vous ferez figurer les réponses aux questions ainsi que tout élément que vous jugerez bon de préciser. Vous déposerez sur Moodle, dans une archive au format *.tar.gz* : 

* le document synthétique au format électronique *.pdf* ;
* les sources dûment commentées de votre réalisation en langage C, ce qui inclut notamment un fichier *Makefile* mais pas les fichiers objets *.o*. **Un code qui ne compile pas ne sera pas accepté ;**

L’ensemble des programmes ayant servi à l’élaboration du projet doit être dûment commenté. Essayez d’attacher une grande importance à la lisibilité du code : il est indispensable de respecter les conventions d’écriture (*coding style*) du langage C et de bien présenter le code (indentation, saut de ligne, etc.) ; les identificateurs (nom de types, variables, fonctions, etc.) doivent avoir des noms cohérents documentant leur rôle ; indiquez les pré-conditions, post-conditions, paramètres, la sortie et un descriptif de chaque fonction. Un commentaire se doit d’être sobre, concis et aussi précis que possible. Pensez à utiliser des outils comme *gdb* ou *valgrind* pour faciliter votre développement et optimiser votre programme.

### Barème indicatif

* Code (programmation et algorithmique) : 12 points
  * Structures de données : 7 points
  * Mise en application : 5 points
* Code (forme, commentaires) : 2 points
* Rapport : 6 points
* Bonus/Malus rendu des TPs : de +2 à -2 points

## Problème

On se propose d’étudier dans ce projet une méthode de compression, et de décompression, d'un fichier au format texte en utilisant plusieurs structures de données différentes.

L'objectif principal de cette étude sera notamment de juger des impacts relatifs au choix des structures de données dans une application.

### Approche proposée

La méthode à analyser sera l'algorithme de compression *LZW* (*Lempel-Ziv-Welch*, du nom de ses auteurs). En plus du bon fonctionnement de la compression des fichiers votre analyse de cette algorithme s'axera sur la structure de données qu'il utilise , à savoir un dictionnaire. Il vous sera demandé de trouver une méthode de comparaison entre chacune des structures proposées. Voici quelques pistes à explorer, mais celles-ci ne sont bien évidement pas exhaustives :

* Complexité en temps
* Complexité en espace
* Facilité de compréhension et temps d'implémentation de la structure

### Algorithme

#### Fonction de compression

```
FONCTION LZW_Compresser(Texte, dictionnaire)

    w ← ""

    TANT QUE (il reste des caractères à lire dans Texte) FAIRE
       c ← Lire(Texte)
       p ← Concaténer(w, c)
       SI Existe(p, dictionnaire) ALORS
          w ← p
       SINON
          Ajouter(p, dictionnaire)
          Écrire dictionnaire[w]
          w ← c
    FIN TANT QUE
```

#### Fonction de décompression

```
FONCTION LZW_Décompresser(Code, dictionnaire)

     n ← |Code|
     v ← Lire(Code)
     Écrire dictionnaire[v]
     w ← chr(v)

     POUR i ALLANT DE 2 à n FAIRE
        v ← Lire(Code)
        SI Existe(v, dictionnaire) ALORS
          entrée ← dictionnaire[v]
        SINON entrée ← Concaténer(w, w[0])
        Écrire entrée
        Ajouter(Concaténer(w,entrée[0]), dictionnaire)
        w ← entrée
     FIN POUR
```



## Implémentation du dictionnaire

### Liste chaînée



### Trie



### Hashmap



## Ressources et aides



## Questions

