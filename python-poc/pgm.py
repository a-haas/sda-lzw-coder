def read_pgm(pgmf):
    # Return a raster of integers from a PGM as string
    assert pgmf.readline() == b'P5\n'
    raster = 'P5\n'
    (width, height) = [int(i) for i in pgmf.readline().split()]
    raster += str(width) + ' ' + str(height) + '\n'
    depth = int(pgmf.readline())
    assert depth <= 255
    raster += str(depth) + '\n'

    for y in range(height):
        for x in range(width):
            raster += (chr(ord(pgmf.read(1))))

    return raster