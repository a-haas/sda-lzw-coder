import lzw

# Compresses "the-adventures-of-sherlock-holmes.txt" into "the-adventures-of-sherlock-holmes.txt.lzw".
with open('assets/the-adventures-of-sherlock-holmes.txt') as input_file:
    with open('assets/the-adventures-of-sherlock-holmes.txt.lzw', 'wb') as compressed_file:
        compressed_file.write(lzw.compress(input_file.read()))

# Decompresses and prints "the-adventures-of-sherlock-holmes.txt.lzw" content.
with open('assets/the-adventures-of-sherlock-holmes.txt.lzw', 'rb') as compressed_file:
    print(lzw.decompress(compressed_file.read()))