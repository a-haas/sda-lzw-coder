#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#include "utils.h"
#include "hex.h"
#include "lzw.h"

Hashmap init_dict() {
    const unsigned initial_size = 65536;
    Hashmap hashmap = malloc(sizeof(struct hashmap_s));
    hashmap_create(initial_size, hashmap);

    return hashmap;
}

void lzw_encode(FILE *in, FILE *out) {
    Hashmap dict = init_dict();
    unsigned dict_size;
    for(dict_size=0; dict_size < 128; dict_size++) {
        char *hex = dec2hex(dict_size, NULL, 8, false);
        char *key = char2str((char) dict_size);
        hashmap_put(dict, key, strlen(key), hex);
    }

    char c;
    char *phrase = malloc(2 * sizeof(char));
    while ((c = fgetc(in)) != EOF) {
        char *next = char2str(c);
        char *tmp = concat(phrase, next);
        
        if(hashmap_get(dict, tmp, strlen(tmp)) != NULL) {
            phrase = tmp;
        }
        else {
            char* hex_code = hashmap_get(dict, phrase, strlen(phrase)); 
            wb_hex_as_int(out, hex_code);

            hashmap_put(dict, tmp, strlen(tmp), dec2hex(dict_size++, NULL, 8, false));
            phrase = next;
        }
    }
    
    wb_hex_as_int(
        out,
        hashmap_get(dict, phrase, strlen(phrase))
    );
}

void lzw_decode(FILE *in, FILE *out) {
    
    Hashmap dict = init_dict();

    // init dict
    unsigned dict_size = 0;
    for(dict_size=0; dict_size < 128; dict_size++) {
        char *hex = dec2hex(dict_size, NULL, 8, false);
        char *value = char2str((char) dict_size);
        hashmap_put(dict, hex, strlen(hex), value);
    }

    unsigned int input;
    char *old = rb_next_int_as_hex(in);
    fputs(hashmap_get(dict, old, strlen(old)), out);

    char *s;
    char *c;
    char *new = rb_next_int_as_hex(in);
    while(new != NULL) {
        if(hashmap_get(dict, new, strlen(new)) == NULL) {
            s = concat(hashmap_get(dict, old, strlen(old)), c);
        }
        else {
            s = hashmap_get(dict, new, strlen(new));
        }

        fputs(s, out);
        c = char2str(s[0]);
        char *tmphex = dec2hex(dict_size++, NULL, 8, false);
        hashmap_put(
            dict, 
            tmphex, 
            strlen(tmphex), 
            concat(hashmap_get(dict, old, strlen(old)), c)
        );
        old = new;

        new = rb_next_int_as_hex(in);
    }
}