#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <stdbool.h>
#include <math.h>

#include "lzw.h"

int main(int argc, char *argv[]) {

    lzw_encode(
        fopen("assets/the-adventures-of-sherlock-holmes.txt", "r"),
        fopen("assets/the-adventures-of-sherlock-holmes.txt.lzw", "wb")
    );

    lzw_decode(
        fopen("assets/the-adventures-of-sherlock-holmes.txt.lzw", "rb"),
        fopen("assets/the-adventures-of-sherlock-holmes-decoded.txt", "w")
    );

    return 0;
}