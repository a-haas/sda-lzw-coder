#ifndef __LZW_H
#define __LZW_H

#include <stdio.h>
#include "hashmap.h"

typedef struct hashmap_s *Hashmap;

Hashmap init_dict();
void lzw_encode(FILE* in, FILE* out);
void lzw_decode(FILE* in, FILE* out);

#endif
