#include <stdlib.h>
#include <string.h>
#include "list.h"

List new_list(char* key, void* value) {
    List list = malloc(sizeof(struct list));
    list->key = key;
    list->value = value;
    list->next = NULL;

    return list;
}

void free_list(List l) {
    if(l == NULL) {
        return;
    }

    free_list(l->next);
    free(l);
}

List add_to_list(List list, char* key, void* value) {
    if(list == NULL) {
        return new_list(key, value);
    }
    
    List current = list;
    while(current->next != NULL) {
        current = current->next;
    }
    current->next = new_list(key, value);

    return list;
}

List remove_from_list(List list, char* key) { 
    List prev = NULL;
    List current = list; 
  
    // If head node itself holds the key to be deleted 
    if (current != NULL && strcmp(current->key, key))  { 
        List next = current->next;
        free(current); // free old head 
        return next;
    }
  
    // Search for the key to be deleted, keep track of the 
    // previous node as we need to change 'prev->next' 
    while (current != NULL && current->key != key) 
    {
        prev = current;
        current = current->next; 
    }
  
    // If key was not present in linked list 
    if (current == NULL) {
        return list;
    }
  
    // Unlink the node from linked list 
    prev->next = current->next;
    free(current);  // Free memory

    return list;
}

void* get_from_key(List list, char* key) {
    List current = list;
    while(current != NULL) {
        if(strcmp(current->key, key) == 0) {
            return current->value;
        }
        current = current->next;
    }
    return NULL;
}