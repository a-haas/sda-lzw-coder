#ifndef __LIST_H
#define __LIST_H

#include <stdbool.h>

typedef struct list {
    char* key;
    void* value;
    struct list* next;
} *List;

List new_list(char* key, void* value);
void free_list(List l);
List add_to_list(List list, char* key, void* value);
List remove_from_list(List list, char* key);
bool is_in_list(List list, char* key);
void* get_from_key(List list, char* key);

#endif