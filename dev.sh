#!/bin/sh

APP_NAME="dev:sda-lzw-coder"
PATH_IN_LOCAL_ENV=/home/ahaas/workspace/sda-lzw-coder

docker build -t $APP_NAME . --file $PATH_IN_LOCAL_ENV/Dockerfile
docker run -it -P -v $PATH_IN_LOCAL_ENV:/app $APP_NAME