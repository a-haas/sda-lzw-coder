# SDA LZW ENCODER/DECODER

## Getting started
As a prerequisite you must have Docker installed (or you can run it locally based on the dependencies listed in the Dockerfile).

To run the app and encode and decode the Sherlock Holmes text file. You can change the file encoded / decoded in the main.

```
./dev.sh # to connect to the docker dev env
make # run make to install & build the project
./build/a.out # to run the app 
```

## Python POC
A Python POC is also available (for quick & dirty testing of the algorithm). To run it follow this steps :

```
./dev.sh # to connect to the docker dev env
python3 ./python-poc/main.py # to run the app 
```

## Subject
The project subject source is available in the `subject.md` file at the root of the file.